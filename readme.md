wasi と node.js の速度比較

標準出力にたくさん書き込むだけ  
大きな計算はナシの場合

1, 2 回目は wasmtime, 3 回目は node + js, 4 回目は node + wasm

```sh
% sh ./scripts/time.sh
   Compiling wasi v0.1.0 (/Users/zono/git/yt-practice/wasi-performance-20220206/wasi)
    Finished release [optimized] target(s) in 0.85s
[INFO]: 🎯  Checking for the Wasm target...
[INFO]: 🌀  Compiling to Wasm...
    Finished release [optimized] target(s) in 0.04s
⚠️   [WARN]: origin crate has no README
[INFO]: ⬇️  Installing wasm-bindgen...
[INFO]: Optimizing wasm binaries with `wasm-opt`...
[INFO]: Optional fields missing from Cargo.toml: 'description', 'repository', and 'license'. These are not necessary, but recommended
[INFO]: ✨   Done in 0.51s
[INFO]: 📦   Your wasm pkg is ready to publish at /Users/zono/git/yt-practice/wasi-performance-20220206/wasm/pkg.
⚠️   [WARN]: There's a newer version of wasm-pack available, the new version is: 0.10.2, you are using: 0.9.1. To update, navigate to: https://rustwasm.github.io/wasm-pack/installer/

real	0m0.148s
user	0m0.184s
sys	0m0.055s

real	0m0.064s
user	0m0.028s
sys	0m0.036s

real	0m0.151s
user	0m0.123s
sys	0m0.048s

real	0m0.185s
user	0m0.176s
sys	0m0.057s
```
