set -Ceu

(
	cd wasi
	cargo build --release
)

(
	cd wasm
	wasm-pack build --target nodejs
)

time wasmtime wasi/target/wasm32-wasi/release/wasi.wasm >| tmp.txt
time wasmtime wasi/target/wasm32-wasi/release/wasi.wasm >| tmp.txt
time node node/index.js >| tmp2.txt
time node wasm/index.js >| tmp3.txt
