#[cfg(feature = "wee_alloc")]
#[global_allocator]
static ALLOC: wee_alloc::WeeAlloc = wee_alloc::WeeAlloc::INIT;

use wasm_bindgen::prelude::*;

#[wasm_bindgen]
extern "C" {
	#[wasm_bindgen(js_namespace = console, js_name = log)] fn print(s: &str);
}

#[wasm_bindgen(js_name = main)]
pub fn main() {
	for i in 0..10000 {
    print(&format!("{}", i));
  }
}
